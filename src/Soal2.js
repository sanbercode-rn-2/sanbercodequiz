import React, { useState, createContext } from 'react'
import {FlatList,View,Text} from "react-native"
export const RootContext = createContext();

const ContextAPI = () => {

    const [ name, setName ] = useState([
        {
            name: 'Zakky Muhammad Fajar',
            position: 'Trainer 1 React Native Lanjutan'
        },
        {
            name: 'Mukhlis Hanafi',
            position: 'Trainer 2 React Native Lanjutan'
        }
    ])

    return (
        <RootContext.Provider value={name}>
            <FlatList 
            data={name}
            renderItem={({item}) => {
                <View>
                    <Text>{`Name : ${item.name}`}</Text>
                    <Text>{`Position : ${item.position}`}</Text>
                </View>
            }}
            keyExtractor={(item) => item.name.toString()}
            />
        </RootContext.Provider>
    )
}

export default ContextAPI;
