import React,{useState,useEffect} from "react";
import { 
    View,
    Text,
    StyleSheet
} from "react-native";

const Soal1 = (props) => {
    const [state,setState] = useState("Jhon Doe")

    useEffect(() => {
        setTimeout(() => {
            setState("Asep")
        })
    })
    return (
    <View style={styles.container}>
        <Text>{state}</Text>
    </View>
    )}
export default Soal1;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});